var Redis = require('../redis')

var redisChannelName = 'jobsSubmittedChannel';

var subscriber = Redis.createClient();
var publisher = Redis.createClient();

subscriber.subscribe(redisChannelName);

module.exports.informSubmittedJob = (job) => {
  publisher.publish(redisChannelName, job.id);
}

module.exports.registerForSubmittedJobAlert = (cb) => {
  subscriber.on('message', (channel, id) => {
    if(channel != redisChannelName)
      return;

    cb(parseInt(id));
  });
}

module.exports.redisChannelName = redisChannelName;
