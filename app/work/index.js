var sleep = require('sleep');

var minSleep = parseInt(process.env.WORK_MIN_DURATION_SECONDS) || 1;
var maxSleep = parseInt(process.env.WORK_MAX_DURATION_SECONDS) || 3;

var interval = (maxSleep - minSleep);

module.exports.doJob = (job) => {
  var sleepTime = Math.ceil((minSleep + Math.random()*interval)*1000000);
  console.log('[Worker '+ process.pid + ']: Sleep for ' + sleepTime/1000 +'ms on job ' + job.id);
  sleep.usleep(sleepTime);
}
