var bluebird = require('bluebird');

var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

var redisHost = process.env.REDIS_SERVER_HOST || 'localhost';
var redisPort = process.env.REDIS_SERVER_PORT || 6379;

module.exports.createClient = () => {
  return redis.createClient(redisPort, redisHost);
}
