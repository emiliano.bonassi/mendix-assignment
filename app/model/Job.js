var Redis = require('../redis');

var redisJobIdKey = 'jobId';
var redisJobsQueuePendingName = 'jobsQueuePending';
var redisJobsQueueInProgressAndCompleted = 'redisJobsQueueInProgressAndCompleted';

var client = Redis.createClient();

var STATE = {
  PENDING: 'PENDING',
  IN_PROGRESS: 'IN_PROGRESS',
  WORKING: 'WORKING',
  COMPLETED: 'COMPLETED'
}

function Job(id, priority, submittedTime) {
  this.id = id;
  this.priority = priority;
  this.submittedTime = submittedTime;
}

var submitJob = (priority) => {
  return client.incrAsync(redisJobIdKey).then((jobId) => {

    var job = new Job(jobId, priority, Date.now());

    job.state = STATE.PENDING;

    client.zadd(redisJobsQueuePendingName, -priority, JSON.stringify(job));
    //client.zadd(redisJobsQueuePendingName, priority, JSON.stringify(job));

    return job;
  });
}

var popJob = () => {
  var multi = client.multi();

  /*multi.zrevrange(redisJobsQueuePendingName, 0, 0)
  .zremrangebyrank(redisJobsQueuePendingName, -1, -1)*/
  multi.zrange(redisJobsQueuePendingName, 0, 0)
  .zremrangebyrank(redisJobsQueuePendingName, 0, 0)

  return multi.execAsync().then((res) => {

    var jobStr = res[0];
    var popped = res[1] == 1 ? true : false;

    return popped ? JSON.parse(jobStr) : null;
  }).then((job) => {

    if(!job || job === undefined)
      return null;

    job.state = STATE.IN_PROGRESS;

    return client.hsetAsync(redisJobsQueueInProgressAndCompleted, job.id, JSON.stringify(job))
                 .then(() => job);
  })
}

var pushCompletedJob = (job) => {
    job.state = STATE.COMPLETED;
    return pushJob(job);
}

var pushWorkingJob = (job) => {
    job.state = STATE.WORKING;
    return pushJob(job);
}

var pushJob = (job) => {
  return client.hsetAsync(redisJobsQueueInProgressAndCompleted, job.id, JSON.stringify(job));
}

var retrieveAllJobs = () => {
  var multi = client.multi();

  multi.zrange(redisJobsQueuePendingName, 0, -1)
       .hvals(redisJobsQueueInProgressAndCompleted);

  return multi.execAsync().then((res) => {

    var jobsPendingStr = res[0];
    var jobsInProgressAndCompletedStr = res[1];

    var jobs = jobsPendingStr.map(jobPendingStr =>  JSON.parse(jobPendingStr))
               .concat(
                 jobsInProgressAndCompletedStr.map(jobInProgressAndCompletedStr => JSON.parse(jobInProgressAndCompletedStr))
               );

    return jobs;
  });
}

var retrieveAllPendingJobs = () => {
  return client.zrangeAsync(redisJobsQueuePendingName, 0, -1)
               .then(jobsPendingStr => jobsPendingStr.map(jobPendingStr =>  JSON.parse(jobPendingStr)));
}

var retrieveAllInProgressCompletedJobs = () => {
  return client.hvalsAsync(redisJobsQueueInProgressAndCompleted)
               .then(jobsInProgressAndCompletedStr => jobsInProgressAndCompletedStr.map(jobInProgressAndCompletedStr =>  JSON.parse(jobInProgressAndCompletedStr)))
}

var retrieveAllInProgressJobs = () => {
  return retrieveAllInProgressCompletedJobs().then(jobs => jobs.filter(job => job.state == STATE.IN_PROGRESS));
}

var retrieveAllCompletedJobs = () => {
  return retrieveAllInProgressCompletedJobs().then(jobs => jobs.filter(job => job.state == STATE.COMPLETED));
}

var deleteAll = () => {
  var multi = client.multi();

  multi.del(redisJobIdKey)
       .del(redisJobsQueuePendingName)
       .del(redisJobsQueueInProgressAndCompleted);

  return multi.execAsync();
}

module.exports = {
  STATE: STATE,
  submitJob: submitJob,
  popJob: popJob,
  pushCompletedJob: pushCompletedJob,
  pushWorkingJob: pushWorkingJob,
  retrieveAllJobs: retrieveAllJobs,
  retrieveAllPendingJobs: retrieveAllPendingJobs,
  retrieveAllInProgressCompletedJobs: retrieveAllInProgressCompletedJobs,
  retrieveAllInProgressJobs: retrieveAllInProgressJobs,
  retrieveAllCompletedJobs: retrieveAllCompletedJobs,
  deleteAll: deleteAll
}
