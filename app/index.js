'use strict';

var cluster = require('cluster');
var JobModel = require('./model').jobModel;
var Communication = require('./communication');

var serverPort = process.env.WEBPORT || 8080;
var numWorkers = process.env.NUM_WORKERS || 1;
var deleteJobs = process.env.DELETE_JOBS ? true : false;

var workers = [];

var WORKER_STATUS = {
  BUSY: 'BUSY',
  PENDING: 'PENDING',
  FREE: 'FREE'
}

function registerWorker(worker) {
  workers.push({worker: worker, status: WORKER_STATUS.FREE})
}

function unregisterWorker(worker) {

  var index = workers.findIndex(wk => wk.worker.process.pid == worker.process.pid);

  if (index != -1)
    workers = workers.splice(index, 1);
}

var lastWorkerUsed = -1;
function findFirstWorkerAvailable()
{
  lastWorkerUsed = (++lastWorkerUsed)%workers.length;

  return workers[lastWorkerUsed].worker;
}

/*function communicateToMaster(status){
  process.send({id: process.pid, status: status});
}*/

function communicateToWorkerNewJob(worker){
  worker.send('new job available!');
}

if (cluster.isMaster) {

  if(deleteJobs)
    JobModel.deleteAll().then(() => console.log("[Master] Deleted all jobs"));

  console.log('[Master] Cluster setting up ' + numWorkers + ' workers...');

  for (var i = 0; i < numWorkers; i++) {
    console.log('[Master] Starting a new worker ' + (i+1) + ' of ' + numWorkers);
    cluster.fork();
  }

  cluster.on('online', function(worker) {
    registerWorker(worker);
    console.log('[Worker ' + worker.process.pid + ']: I\'m online');

    /*worker.on('message', function(message) {
      var sender = workers.find(wk => wk.worker.process.pid == message.id);

      if(sender != undefined)
        sender.status = message.status;
    });*/
  });

  cluster.on('exit', function(worker, code, signal) {
    unregisterWorker(worker);
    console.log('[Worker ' + worker.process.pid + ']: I\'m died with code: ' + code + ', and signal: ' + signal);

    console.log('Starting a new worker');
    cluster.fork();
  });

  var app = require('express')();
  var swaggerTools = require('swagger-tools');
  var jsyaml = require('js-yaml');
  var fs = require('fs');

  // swaggerRouter configuration
  var options = {
    swaggerUi: '/swagger.json',
    controllers: './controllers',
    useStubs: process.env.NODE_ENV === 'development' ? true : false // Conditionally turn on stubs (mock mode)
  };

  // The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
  var spec = fs.readFileSync('./api/swagger.yaml', 'utf8');
  var swaggerDoc = jsyaml.safeLoad(spec);

  // Initialize the Swagger middleware
  swaggerTools.initializeMiddleware(swaggerDoc, function(middleware) {
    // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
    app.use(middleware.swaggerMetadata());

    // Validate Swagger requests
    app.use(middleware.swaggerValidator());

    // Route validated requests to appropriate controller
    app.use(middleware.swaggerRouter(options));

    // Serve the Swagger documents and Swagger UI
    app.use(middleware.swaggerUi());


    // Start the server
    app.listen(serverPort, function() {
      console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
      console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);
  });
  });

  Communication.registerForSubmittedJobAlert(id => {
    console.log('[Master] Received alert for job ' + id);
    var worker = findFirstWorkerAvailable();

    /*if(worker == null)
      return console.log('[Master] No free worker available at the moment');*/

    console.log('[Master] Worker ' + worker.process.pid + ' selected');
    communicateToWorkerNewJob(worker);

  });

}
else{
  var Work = require('./work');

    var latestPromise = null;

    process.on('message', (message) => {
      console.log('[Worker '+ process.pid + ']: Received alert: ' + message);

      if(latestPromise == null)
        latestPromise = JobModel.popJob();
      else
        latestPromise = latestPromise.then(JobModel.popJob);

        latestPromise.then(job => {

                if(!job)
                {
                  console.log('[Worker '+ process.pid + ']: No job popped out');
                  //communicateToMaster(WORKER_STATUS.FREE);
                  return;
                }

                job.startedTime = Date.now();
                JobModel.pushWorkingJob(job);
                console.log('[Worker '+ process.pid + ']: Started working on job ' + job.id);
                /*communicateToMaster(WORKER_STATUS.BUSY)
                console.log('[Worker '+ process.pid + ']: I\'m busy');*/

                Work.doJob(job);

                job.completionTime = Date.now();
                console.log('[Worker '+ process.pid + ']: Finished working on job ' + job.id);

                JobModel.pushCompletedJob(job).then(() => {
                  /*console.log('[Worker '+ process.pid + ']: I\'m free');
                  communicateToMaster(WORKER_STATUS.FREE);*/
                })
              });
    });
}
