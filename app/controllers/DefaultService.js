'use strict';

var JobModel = require('../model').jobModel;
var Communication = require('../communication');

exports.listJobs = function(args, res, next) {
  /**
   * parameters expected in the args:
  * sortBy (String)
  **/
  JobModel.retrieveAllJobs()
          .then(jobs => {

            if(args.sortBy.value)
            {
              jobs.sort((a,b) => {
                var valueA = a[args.sortBy.value];
                var valueB = b[args.sortBy.value];

                if((valueA == undefined && valueB == undefined))
                  return 0;
                else if (valueA == undefined || (valueA - valueB < 0))
                  return -1;
                else if (valueB == undefined || (valueA - valueB > 0))
                  return +1;
                else
                  return 0;
              })
            }
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(jobs || {}, null, 2));
          })
          .catch(err => {
            res.status(500).send("Error: " + err);
          })
}

exports.submitJobWithPriority = function(args, res, next) {
  /**
   * parameters expected in the args:
  * priority (Integer)
  **/
  JobModel.submitJob(args.priority.value)
          .then(job => {
            res.setHeader('Content-Type', 'text/html');
            res.send(200, job.id);
            return job;
          })
          .then(job => {
            console.log('[Master] Just sumbmitted job with id ' + job.id);
            Communication.informSubmittedJob(job);
          })
          .catch(err => {
            res.status(500).send("Error: " + err);
          })
}
