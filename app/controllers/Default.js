'use strict';

var url = require('url');


var Default = require('./DefaultService');


module.exports.listJobs = function listJobs (req, res, next) {
  Default.listJobs(req.swagger.params, res, next);
};

module.exports.submitJobWithPriority = function submitJobWithPriority (req, res, next) {
  Default.submitJobWithPriority(req.swagger.params, res, next);
};
