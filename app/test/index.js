var expect = require('chai').expect;
var assert = require('chai').assert;
var Model = require('../model');
var Communication = require('../communication');
var Redis = require('../redis');

describe('Assignment', () => {
    describe('Model', () => {
      var JobModel = Model.jobModel;

      beforeEach(done => {
          JobModel.deleteAll().then(() => done())
      })

      afterEach(done => {
          JobModel.deleteAll().then(() => done())
      })

      describe('Job submission and retrival', () => {

        it('should return no jobs if queues are empty', done => {
          JobModel.retrieveAllJobs()
                  .then(jobs => {
                    expect(jobs).to.be.empty;
                  })
                  .then(JobModel.retrieveAllPendingJobs)
                  .then(jobs => {
                    expect(jobs).to.be.empty;
                  })
                  .then(JobModel.retrieveAllInProgressJobs)
                  .then(jobs => {
                    expect(jobs).to.be.empty;
                  })
                  .then(JobModel.retrieveAllCompletedJobs)
                  .then(jobs => {
                    expect(jobs).to.be.empty;
                    done();
                  })
        })

          it('should submit a job with the specified priority', done => {
            var priority = 12;
            JobModel.submitJob(priority).then(job => {
              expect(job.priority).to.be.equal(priority);
              expect(job.state).to.be.equal(Model.jobModel.STATE.PENDING)
              done();
            })
          })

          it('should submit a job with the specified priority and retrieve it', done => {
            var priority = 15;

            var job = {};

            JobModel.submitJob(priority)
            .then(j => {job = j})
            .then(JobModel.retrieveAllJobs)
            .then(jobs => {
              expect(jobs).to.be.not.null
              expect(jobs.length).to.be.equal(1)
              expect(jobs[0].id).to.be.equal(job.id);
              expect(jobs[0].priority).to.be.equal(job.priority);
              expect(jobs[0].state).to.be.equal(Model.jobModel.STATE.PENDING)
              done();
            })
          })

          it('should pop the job from pending queue and push in the inprogress queue', done => {
            var priority = 15;

            var jobP = {};

            JobModel.submitJob(priority)
            .then(job => {jobP = job})
            .then(JobModel.popJob)
            .then(job => {
              expect(job.id).to.be.equal(jobP.id)
              expect(job.priority).to.be.equal(jobP.priority)
              expect(job.state).to.be.equal(Model.jobModel.STATE.IN_PROGRESS)
            })
            .then(JobModel.retrieveAllPendingJobs)
            .then(jobs => {
              expect(jobs).to.be.empty;
            })
            .then(JobModel.retrieveAllInProgressJobs)
            .then(jobs => {
              expect(jobs).to.be.not.empty;
              expect(jobs.length).to.be.equal(1);
              expect(jobs[0].id).to.be.equal(jobP.id);
              expect(jobs[0].priority).to.be.equal(jobP.priority);
              expect(jobs[0].state).to.be.equal(Model.jobModel.STATE.IN_PROGRESS)
            })
            .then(JobModel.retrieveAllCompletedJobs)
            .then(jobs => {
              expect(jobs).to.be.empty;
            })
            .then(JobModel.retrieveAllInProgressCompletedJobs)
            .then(jobs => {
              expect(jobs).to.be.not.empty;
              expect(jobs.length).to.be.equal(1);
              expect(jobs[0].id).to.be.equal(jobP.id);
              expect(jobs[0].priority).to.be.equal(jobP.priority);
              expect(jobs[0].state).to.be.equal(Model.jobModel.STATE.IN_PROGRESS)
              done();
            })

          })


          it('should pop the job with the highest priority', done => {
            var priorityHighest = 3;
            var priorityOther = 2;
            var priorityLowest = -2;

            var jobHighest = {};

            JobModel.submitJob(priorityLowest)
                    .then(JobModel.submitJob(priorityHighest))
                    .then(job => {jobHighest = job})
                    .then(JobModel.submitJob(priorityOther))
                    .then(JobModel.popJob)
                    .then(job => {
                              expect(job.id).to.be.equal(jobHighest.id)
                              done();
                          });
          })

          it('should pop the job from pending queue, complete it and push in the completed queue', done => {
            var priority = 15;

            var jobP = {};

            JobModel.submitJob(priority)
            .then(job => {jobP = job})
            .then(JobModel.popJob)
            .then(JobModel.pushCompletedJob)
            .then(JobModel.retrieveAllPendingJobs)
            .then(jobs => {
              expect(jobs).to.be.empty;
            })
            .then(JobModel.retrieveAllCompletedJobs)
            .then(jobs => {
              expect(jobs).to.be.not.empty;
              expect(jobs.length).to.be.equal(1);
              expect(jobs[0].id).to.be.equal(jobP.id);
              expect(jobs[0].priority).to.be.equal(jobP.priority);
              expect(jobs[0].state).to.be.equal(Model.jobModel.STATE.COMPLETED)
            })
            .then(JobModel.retrieveAllInProgressJobs)
            .then(jobs => {
              expect(jobs).to.be.empty;
            })
            .then(JobModel.retrieveAllInProgressCompletedJobs)
            .then(jobs => {
              expect(jobs).to.be.not.empty;
              expect(jobs.length).to.be.equal(1);
              expect(jobs[0].id).to.be.equal(jobP.id);
              expect(jobs[0].priority).to.be.equal(jobP.priority);
              expect(jobs[0].state).to.be.equal(Model.jobModel.STATE.COMPLETED);
              done();
            })

          })
        })
      })
      describe('Communication', () => {
        it('should receive submitted job alerts', done => {

          var job = {id: 13};

          Communication.registerForSubmittedJobAlert((message) => {
            expect(message).to.be.equal(job.id);
            done();
          })

          Communication.informSubmittedJob(job);

        })
      });
    })
