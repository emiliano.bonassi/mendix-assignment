#!/bin/bash

export REDIS_SERVER_HOST=192.168.99.100
export REDIS_SERVER_PORT=6379
export NUM_WORKERS=2
export DELETE_JOBS=true
export WORK_MIN_DURATION_SECONDS=1
export WORK_MAX_DURATION_SECONDS=3

node index.js
