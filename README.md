# Programming assignment

You are tasked to implement a REST API with the following requirements:
- `POST /jobs?priority=NUMBER` creates a job within the system with priority NUMBER (< 0 lower priority, = 0 default, > 0 higher priority). The ID of the created job is returned as response.
- `GET /jobs` list all jobs with their states, priority, start time and completion (at least these attributes)
- Each job requires 1-3 seconds to complete (randomize the duration and simulate work with `sleep`)
- N Workers: The system can process N (to be set during program startup) jobs concurrently respecting priority
- At least 1 unittest
- The service runs on Linux
- Bonus points if GET supports e.g. `/jobs?sortBy=completion`
- Bonus points if your implementation is horizontally scalable: imagine there's a HTTP load balancer in front of your application with 5 running instances.

You are free to choose your own programming language and framework.


## Solution

1. what you need to run it
2. how to run and test it
3. the general idea
4. which frameworks/tools i’ve used
5. comments/bugs/TODOs

1.
It is based on node.js and it requires a Redis server instance.

You must setup the Redis server hostname and port as environment variables in the file app/run.sh
If you want you can start an instance of Redis using docker with the script run_docker_redis.sh

2.
In app/run.sh you also find other environment variables to customize behavior.
Using app/build.sh you may install all the dependancies required.
With app/run.sh you may run it and with app/test.sh you may execute some unit tests.

3.
In few words the application works by this way.
There are N+1 processes (thanks to node.js cluster), the first(master) is the one which exposes the REST API using express and submits the jobs in a ordered set on the Redis server. After the process submit a job, it publishes a message on a well defined Redis channel.
The other N processes are the workers, they have a subscription to the Redis channel so all of them receive a message every time a job is submitted. When one of them receives a message, it pop/extract from the ordered set the oldest job with the highest priority. After the extraction it change the state of the job from PENDING to IN_PROGRESS and put it in the hash set of IN_PROGRESS/COMPLETED jobs.
Then it simulates work and when it completes the job, it changes the state to COMPLETED and adds information about the start and end time, putting it in the previous hash set.

4.
For the REST APIs, I’ve used Swagger. It helps me to describe as a code the API and it may be used to generate server/client code for several platforms/framework. In addition, it gives for free parameters validation.

For concurrency, I’ve used cluster. It is the only choice for node.js . I could use another programming language (like Java with Vert.x/Spring) but I preferred node.js for development speed.

For data backend, I thought two options: local structure shared between workers/processes, MongoDB with in memory db or Redis. I opted for Redis because it has natural data structures for represent this kind of problems. In addition, it respect to a local structure it permits to scale horizontally in a easier way.
